-- |
-- Module      : HaskenGL
-- Description : OpenGL library with compiler safety baked in
-- Copyright   : (c) Alexandre Levy, 2021
-- License     : BSD-3 Clause
-- Maintainer  : a13xlevy@pm.me
-- Stability   : experimental
-- Portability : POSIX
--
-- OpenGL library for Haskell with compiler safety mechanisms baked in. For instance if you try to allocate a VBO without a bound VAO then then compilation fails
-- rather than getting a runtime error, or worse nothing displayed.
--
-- The library uses `IxMonad` to track a state of OpenGL objects at compile time with the help of "Data.Singletons" to define type-level equations.
--
-- Check out example scenes in `examples` folder for some basic scenes implemented using `HaskenGL`.
module Graphics.HaskenGL
  ( module Graphics.HaskenGL.SafeArrays,
    module Graphics.HaskenGL.SafeBuffers,
    module Graphics.HaskenGL.SafePrograms,
    module Graphics.HaskenGL.SafeUniforms,
    module Graphics.HaskenGL.Linear,
    module Graphics.HaskenGL.Tracker,
    module Graphics.HaskenGL.Internal.Types,
    module Graphics.HaskenGL.Window,
    module Graphics.HaskenGL.SafeTextures,
    module Graphics.HaskenGL.Camera,
    module Graphics.HaskenGL.Image,
  )
where

import Graphics.HaskenGL.Camera
import Graphics.HaskenGL.Image
import Graphics.HaskenGL.Internal.Types
import Graphics.HaskenGL.Linear
import Graphics.HaskenGL.SafeArrays
import Graphics.HaskenGL.SafeBuffers
import Graphics.HaskenGL.SafePrograms
import Graphics.HaskenGL.SafeTextures
import Graphics.HaskenGL.SafeUniforms
import Graphics.HaskenGL.Tracker
import Graphics.HaskenGL.Window
