module Graphics.HaskenGL.Linear
  ( translate,
    rotate,
    scale,
    glMatrix,
  )
where

import Graphics.Rendering.OpenGL.GL qualified as GL
import Linear (M44, Quaternion, V3 (V3), V4 (V4), fromQuaternion)

scale :: (Floating a) => V3 a -> M44 a
scale (V3 x y z) = V4 (V4 x 0 0 0) (V4 0 y 0 0) (V4 0 0 z 0) (V4 0 0 0 1)

translate :: (Floating a) => V3 a -> M44 a
translate (V3 x y z) = V4 (V4 1 0 0 x) (V4 0 1 0 y) (V4 0 0 1 z) (V4 0 0 0 1)

rotate :: (Floating a) => Quaternion a -> M44 a
rotate q = let (V3 r1 r2 r3) = fromQuaternion q in V4 (snoc3 r1 0) (snoc3 r2 0) (snoc3 r3 0) (V4 0 0 0 1)
  where
    snoc3 :: V3 a -> a -> V4 a
    snoc3 (V3 x y z) = V4 x y z

glMatrix :: (GL.MatrixComponent a) => M44 a -> IO (GL.GLmatrix a)
glMatrix m = do
  let mArr = foldr (\(V4 x y z w) acc -> x : y : z : w : acc) [] m
  GL.newMatrix GL.RowMajor mArr
