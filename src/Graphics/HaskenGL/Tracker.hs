module Graphics.HaskenGL.Tracker
  ( TrackT (..),
    track,
    trackWithState_,
    tearDown,
    (↑↑),
    (↑!),
    (↑),
    (∙),
    (▶),
    (◆),
    (>>>),
    (>>>=),
    TrackError,
    TrackError',
  )
where

import Control.Lens (Lens')
import Control.Monad (forM_, (<=<))
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Indexed
  ( IxApplicative (..),
    IxFunctor (..),
    IxMonad (..),
    IxPointed (..),
    (>>>=),
  )
import Control.Monad.Indexed.Trans (IxMonadTrans (..))
import Control.Monad.State (MonadState (get), StateT, evalStateT, execStateT)
import Control.Monad.Trans (MonadTrans, lift)
import Data.Map.Strict (Map, elems)
import Data.Singletons.Base.TypeError (ErrorMessage' (ShowType, Text, (:<>:)), TypeError)
import GHC.Base (Type)
import Graphics.HaskenGL.Internal.Types (HasGLState (uniformBuffers), TrackedObject, newGLState, programs, textures, vaos)
import Graphics.Rendering.OpenGL (ObjectName)
import Graphics.Rendering.OpenGL qualified as GL
import Lens.Micro.Extras (view)

type TrackError hdr obj ftr = TypeError ('Text hdr ':<>: 'Text " " ':<>: 'ShowType obj ':<>: 'Text " " ':<>: 'Text ftr)

type TrackError' hdr obj ftr = TypeError ('Text hdr ':<>: 'Text " " ':<>: 'ShowType obj ':<>: 'Text " " ':<>: ftr)

newtype TrackT (m :: Type -> Type) (i :: [TrackedObject]) (o :: [TrackedObject]) a = TrackT {runTrackT :: m a}

instance (Functor m) => Functor (TrackT m i o) where
  fmap f (TrackT a) = TrackT $ f <$> a

instance (Applicative m) => Applicative (TrackT m i o) where
  pure a = TrackT $ pure a
  TrackT f <*> TrackT a = TrackT $ f <*> a

instance (Monad m) => Monad (TrackT m i o) where
  TrackT m >>= f = TrackT $ m >>= runTrackT . f

instance (Functor m) => IxFunctor (TrackT m) where
  imap f (TrackT a) = TrackT $ fmap f a

instance (Applicative m) => IxPointed (TrackT m) where
  ireturn a = TrackT $ pure a

instance (Applicative m) => IxApplicative (TrackT m) where
  iap (TrackT f) (TrackT a) = TrackT $ f <*> a

instance (Monad m) => IxMonad (TrackT m) where
  ibind f (TrackT m) = TrackT $ m >>= (runTrackT . f)

instance IxMonadTrans TrackT where
  ilift = TrackT

track :: (HasGLState st) => TrackT (StateT st IO) '[] o a -> IO a
track t = do
  st <- newGLState
  evalStateT (runTrackT t) st

trackWithState_ :: (MonadIO m) => st -> TrackT (StateT st m) i o a -> m ()
trackWithState_ st = const (pure ()) <=< flip execStateT st . runTrackT

(↑↑) :: (MonadTrans t, Monad (t m), Monad m) => m a -> TrackT (t m) i i a
(↑↑) = ilift . lift

infixl 5 ↑↑

(↑!) :: (MonadIO (t IO)) => IO a -> TrackT (t IO) i i a
(↑!) = ilift . liftIO

infixl 5 ↑!

(↑) :: (Monad (t m)) => t m a -> TrackT (t m) i i a
(↑) = ilift

infixl 5 ↑

(>>>) :: (Monad m) => TrackT m i j a -> TrackT m j k b -> TrackT m i k b
x >>> y = x >>>= const y

infixl 5 >>>

(∙) :: (Monad m) => TrackT m i j a -> TrackT m j k b -> TrackT m i k b
(∙) = (>>>)

infixl 1 ∙

(▶) :: (Monad m) => TrackT m i j a -> (a -> TrackT m j k b) -> TrackT m i k b
(▶) = (>>>=)

infixl 1 ▶

(◆) :: TrackT m '[] j a -> TrackT m '[] j a
(◆) = id

infixl 1 ◆

free :: (ObjectName a, MonadState s m, MonadIO m) => Lens' s (Map String a) -> s -> m ()
free objs s = forM_ (elems $ view objs s) GL.deleteObjectName

tearDown :: (MonadIO m, HasGLState st) => TrackT (StateT st m) i o ()
tearDown = TrackT $ do
  st <- get
  free vaos st
  free programs st
  free textures st
  free uniformBuffers st
