{-# LANGUAGE PartialTypeSignatures #-}

module Graphics.HaskenGL.Window
  ( withWindow,
    capFps,
    mouseCallback,
    frameBufferCallBack,
    keyCallback,
    updateCounters,
    showFps,
  )
where

import Control.Concurrent.STM
  ( TVar,
    atomically,
    modifyTVar,
    readTVar,
    readTVarIO,
    writeTVar,
  )
import Control.Exception (bracket)
import Control.Monad (unless, when)
import Control.Monad.State (StateT)
import Data.Functor ((<&>))
import Data.Maybe (fromJust)
import Data.Set (Set, delete, insert)
import GHC.Float.RealFracMethods (floorDoubleInt, int2Double)
import Graphics.HaskenGL.Internal.Types
  ( CursorPosition (CursorPosition, cursorX, cursorY),
    EulerAngles (EulerAngles),
    GLMajorVersion,
    GLMinorVersion,
    HasGLState (framesByTime, lastFrame),
    Sensitivity,
    WindowHeight,
    WindowTitle,
    WindowWidth,
    frameDelta,
  )
import Graphics.HaskenGL.Tracker (TrackT, (↑!))
import Graphics.Rendering.OpenGL qualified as GL
import Graphics.UI.GLFW qualified as GLFW
import Lens.Micro.Extras (view)

withWindow ::
  GLMajorVersion ->
  GLMinorVersion ->
  WindowWidth ->
  WindowHeight ->
  WindowTitle ->
  (GLFW.Window -> IO ()) ->
  IO ()
withWindow major minor w h t f = do
  r <- GLFW.init
  unless r GLFW.terminate
  GLFW.defaultWindowHints
  GLFW.windowHint $ GLFW.WindowHint'ContextVersionMajor major
  GLFW.windowHint $ GLFW.WindowHint'ContextVersionMinor minor
  bracket
    (GLFW.createWindow w h t Nothing Nothing)
    (const GLFW.terminate)
    $ maybe (fail "Could not create window")
    $ runWindow f

runWindow :: (GLFW.Window -> IO ()) -> GLFW.Window -> IO ()
runWindow f win = do
  GLFW.makeContextCurrent $ Just win
  GLFW.setErrorCallback $ Just simpleErrorCallback
  f win
  GLFW.setWindowShouldClose win True
  GLFW.destroyWindow win

simpleErrorCallback :: GLFW.ErrorCallback
simpleErrorCallback e s = putStrLn $ unwords [show e, show s]

mouseCallback :: Sensitivity -> TVar Double -> TVar CursorPosition -> TVar EulerAngles -> GLFW.CursorPosCallback
mouseCallback sensitivity delta cursor euler _ x y = atomically $ do
  lastCursor <- readTVar cursor
  δ <- readTVar delta
  let xOffset = sensitivity * δ * (x - cursorX lastCursor)
  let yOffset = sensitivity * δ * (cursorY lastCursor - y)
  writeTVar cursor $ CursorPosition x y
  modifyTVar euler $ \(EulerAngles pitch yaw) -> EulerAngles (max (-89) (min 89 (pitch + yOffset))) (yaw + xOffset)

frameBufferCallBack :: GLFW.FramebufferSizeCallback
frameBufferCallBack _ w h = GL.viewport GL.$= (GL.Position 0 0, GL.Size (fromIntegral w) (fromIntegral h))

keyCallback :: TVar (Set GLFW.Key) -> GLFW.KeyCallback
keyCallback keys _ key _ GLFW.KeyState'Pressed _ = atomically $ modifyTVar keys (insert key)
keyCallback keys _ key _ GLFW.KeyState'Repeating _ = atomically $ modifyTVar keys (insert key)
keyCallback keys _ key _ GLFW.KeyState'Released _ = atomically $ modifyTVar keys (delete key)

capFps :: Double -> TrackT (StateT st IO) _ _ () -> TrackT (StateT st IO) _ _ ()
capFps fps f = do
  t0 <- (↑!) $ GLFW.getTime <&> fromJust
  (↑!) $ idle t0 (1 / fps)
  f
  where
    idle :: Double -> Double -> IO ()
    idle t0 maxPeriod = do
      t <- GLFW.getTime <&> fromJust
      unless (t - t0 >= maxPeriod) $ idle t0 maxPeriod

updateCounters :: (HasGLState st) => Double -> st -> IO ()
updateCounters t st = atomically $ do
  let fbt = view framesByTime st
  lf <- readTVar $ view lastFrame st
  let delta = t - lf
  writeTVar (view frameDelta st) delta
  writeTVar (view lastFrame st) t
  (frames, elapsed) <- readTVar fbt
  writeTVar fbt (frames + 1, elapsed + delta)

showFps :: (HasGLState st) => st -> String -> GLFW.Window -> IO ()
showFps st title window = do
  let fbt = view framesByTime st
  (frames, elapsed) <- readTVarIO fbt
  when (elapsed >= 1) $ do
    GLFW.setWindowTitle window $ title <> " (FPS: " <> show (floorDoubleInt $ int2Double frames / elapsed) <> ")"
    atomically $ writeTVar fbt (0, 0)
