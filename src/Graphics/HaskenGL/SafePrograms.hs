module Graphics.HaskenGL.SafePrograms
  ( TrackedProgram (..),
  )
where

import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Indexed (IxMonad)
import Control.Monad.State (MonadState, get, modify)
import Control.Monad.Trans (MonadTrans (lift))
import Data.Map.Strict as M (insert, lookup)
import GHC.Ptr (nullPtr)
import Graphics.HaskenGL.Internal.Types
  ( HasGLState (programs),
    Shader,
    TrackedObject (BoundVertexArrayObject, Program, UsedProgram),
  )
import qualified Graphics.HaskenGL.Shader as HS
import Graphics.HaskenGL.Tracker (TrackError, TrackT (TrackT))
import qualified Graphics.Rendering.OpenGL as GL
import Graphics.Rendering.OpenGL.GL (($=))
import Lens.Micro (over)
import Lens.Micro.Extras (view)
import GHC.TypeLits.Singletons (KnownSymbol, symbolVal)
import Data.Bool.Singletons (If)
import Data.Foldable.Singletons (Elem)
import Data.Singletons (Sing)
import Data.List.Singletons (Delete)

class IxMonad m => TrackedProgram m where
  createProgram ::
    ( KnownSymbol p,
      If (Elem ('Program p) i) (TrackError "Program" p "already exists") 'False ~ 'False,
      Traversable t
    ) =>
    Sing p ->
    t Shader ->
    m i ('Program p ': i) ()
  useProgram ::
    ( KnownSymbol p,
      If (Elem ('Program p) i) 'True (TrackError "Program" p "does not exist") ~ 'True
    ) =>
    Sing p ->
    m i ('UsedProgram p ': Delete ('UsedProgram p) i) ()
  unuseProgram ::
    ( KnownSymbol p,
      If (Elem ('UsedProgram p) i) 'True (TrackError "Program" p "is not in use") ~ 'True
    ) =>
    Sing p ->
    m i (Delete ('UsedProgram p) i) ()
  drawArrays ::
    ( KnownSymbol p,
      KnownSymbol v,
      If (Elem ('UsedProgram p) i) 'True (TrackError "Program" p "is not in use") ~ 'True,
      If (Elem ('BoundVertexArrayObject v) i) 'True (TrackError "VAO" v "is not bound") ~ 'True
    ) =>
    Sing p ->
    Sing v ->
    GL.PrimitiveMode ->
    GL.ArrayIndex ->
    GL.NumArrayIndices ->
    m i i ()
  drawElements ::
    ( KnownSymbol p,
      KnownSymbol v,
      If (Elem ('UsedProgram p) i) 'True (TrackError "Program" p "is not in use") ~ 'True,
      If (Elem ('BoundVertexArrayObject v) i) 'True (TrackError "VAO" v "is not bound") ~ 'True
    ) =>
    Sing p ->
    Sing v ->
    GL.PrimitiveMode ->
    GL.NumArrayIndices ->
    GL.DataType ->
    m i i ()

instance (HasGLState st,
  MonadTrans t, MonadState st (t IO), MonadIO (t IO)) => TrackedProgram (TrackT (t IO)) where
  createProgram p shaders = TrackT $ do
    prog <- lift $ HS.createProgram shaders
    modify $ over programs $ insert (symbolVal p) prog
  useProgram p = TrackT $ do
    st <- get
    GL.currentProgram $= M.lookup (symbolVal p) (view programs st)
  unuseProgram _ = TrackT $ GL.currentProgram $= Nothing
  drawArrays _ _ pm ai nai = TrackT $ lift $ GL.drawArrays pm ai nai
  drawElements _ _ pm nai dt = TrackT $ lift $ GL.drawElements pm nai dt nullPtr
