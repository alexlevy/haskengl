module Graphics.HaskenGL.SafeUniforms
  ( setUniform,
    bindUniformBlock,
    createUniformBuffer,
    fillUniformBuffer,
  )
where

import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Indexed (IxMonad)
import Control.Monad.State (MonadState (get), MonadTrans (lift), modify)
import Data.Bool.Singletons (If)
import Data.Foldable.Singletons (Elem)
import Data.Map.Strict qualified as M
import Data.Maybe (fromJust)
import Data.Singletons (Sing)
import Data.Singletons.Base.TypeError (ErrorMessage' (ShowType, Text, (:<>:)))
import Foreign (Ptr, nullPtr)
import GHC.TypeLits.Singletons (KnownSymbol, symbolVal)
import Graphics.HaskenGL.Internal.Types
  ( BindingPoint,
    HasGLState (uniformBuffers),
    TrackedObject (BoundUniformBlock, Program, UniformBuffer, UsedProgram),
    programs,
  )
import Graphics.HaskenGL.Tracker (TrackError, TrackError', TrackT (TrackT))
import Graphics.Rendering.OpenGL qualified as GL
import Graphics.Rendering.OpenGL.GL (($=))
import Lens.Micro (over)
import Lens.Micro.Extras (view)

class (IxMonad m) => TrackedUniform m where
  setUniform ::
    ( KnownSymbol prog,
      GL.Uniform a,
      If (Elem ('UsedProgram prog) i) 'True (TrackError "Program" prog "is not in use") ~ 'True
    ) =>
    Sing prog ->
    String ->
    a ->
    m i i ()
  bindUniformBlock ::
    ( KnownSymbol prog,
      KnownSymbol block,
      KnownSymbol buffer,
      If (Elem ('Program prog) i) 'True (TrackError "Program" prog "does not exist") ~ 'True,
      If (Elem ('BoundUniformBlock block prog) i) (TrackError' "Uniform block" block ('Text "already bound to program " ':<>: 'ShowType prog)) 'False ~ 'False,
      If (Elem ('UniformBuffer buffer) i) 'True (TrackError "Uniform buffer" buffer "does not exist") ~ 'True
    ) =>
    Sing prog ->
    Sing block ->
    Sing buffer ->
    BindingPoint ->
    Maybe GL.GLsizeiptr ->
    m i ('BoundUniformBlock block prog ': i) ()
  createUniformBuffer ::
    ( KnownSymbol s,
      If (Elem ('UniformBuffer s) i) (TrackError "Uniform buffer" s "already exists") 'False ~ 'False
    ) =>
    Sing s ->
    GL.BufferUsage ->
    GL.GLsizeiptr ->
    m i ('UniformBuffer s ': i) ()
  fillUniformBuffer ::
    ( KnownSymbol buffer,
      If (Elem ('UniformBuffer buffer) i) 'True (TrackError "Uniform buffer" buffer "does not exist") ~ 'True
    ) =>
    Sing buffer ->
    GL.GLintptr ->
    GL.GLsizeiptr ->
    Ptr a ->
    m i i ()

instance (HasGLState st, MonadTrans t, MonadState st (t IO), MonadIO (t IO)) => TrackedUniform (TrackT (t IO)) where
  setUniform progName uniformName uniformVal = TrackT $ do
    st <- get
    let prog = fromJust $ M.lookup (symbolVal progName) (view programs st)
    loc <- lift $ GL.uniformLocation prog uniformName
    GL.uniform loc $= uniformVal
  bindUniformBlock progName blockName bufferName bindingPoint sz = TrackT $ do
    st <- get
    let prog = fromJust $ M.lookup (symbolVal progName) (view programs st)
    let ubo = fromJust $ M.lookup (symbolVal bufferName) (view uniformBuffers st)
    -- Left side
    idx <- lift $ GL.uniformBlockIndex prog (symbolVal blockName)
    GL.uniformBlockBinding prog idx $= bindingPoint
    -- Right side
    case sz of
      Nothing -> GL.bindBufferBase GL.IndexedUniformBuffer bindingPoint $= Just ubo
      Just sz' -> GL.bindBufferRange GL.IndexedUniformBuffer bindingPoint $= Just (ubo, 0, sz')
  createUniformBuffer bufferName usage sz = TrackT $ do
    ubo <- GL.genObjectName
    GL.bindBuffer GL.UniformBuffer $= Just ubo
    GL.bufferData GL.UniformBuffer $= (sz, nullPtr, usage)
    modify $ over uniformBuffers $ M.insert (symbolVal bufferName) ubo
    GL.bindBuffer GL.UniformBuffer $= Nothing
  fillUniformBuffer bufferName offset sz d = TrackT $ do
    st <- get
    let ubo = M.lookup (symbolVal bufferName) (view uniformBuffers st)
    GL.bindBuffer GL.UniformBuffer $= ubo
    liftIO $ GL.bufferSubData GL.UniformBuffer GL.WriteToBuffer offset sz d
    GL.bindBuffer GL.UniformBuffer $= Nothing
