module Graphics.HaskenGL.SafeTextures
  ( TrackedTexture,
    createTexture,
    bindTexture,
    activateTexture,
    unbindTexture,
    textureFilter,
    textureWrapMode,
    loadTexture2D,
    generateMipMap,
  )
where

import Codec.Picture (Image (Image), Pixel (PixelBaseComponent))
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Indexed (IxMonad)
import Control.Monad.State (MonadState, get, modify)
import Data.Bool.Singletons (If)
import Data.Foldable.Singletons (Elem)
import Data.List.Singletons (Delete)
import Data.Map.Strict (insert)
import Data.Map.Strict qualified as M
import Data.Singletons (Sing)
import Data.Vector.Storable (Storable, unsafeWith)
import GHC.TypeLits.Singletons (KnownSymbol, symbolVal)
import Graphics.HaskenGL.Internal.Types (HasGLState (textures), TextureUnit, TrackedObject (BoundTexture, Texture))
import Graphics.HaskenGL.Tracker (TrackError, TrackT (TrackT))
import Graphics.Rendering.OpenGL (($=))
import Graphics.Rendering.OpenGL qualified as GL
import Lens.Micro (over)
import Lens.Micro.Extras (view)

class IxMonad m => TrackedTexture m where
  createTexture ::
    ( KnownSymbol s,
      If (Elem ('Texture s) i) (TrackError "Texture" s "already exists") 'False ~ 'False
    ) =>
    Sing s ->
    m i ('Texture s ': i) ()
  bindTexture ::
    ( KnownSymbol s,
      GL.BindableTextureTarget a,
      If (Elem ('Texture s) i) 'True (TrackError "Texture" s "does not exist") ~ 'True
    ) =>
    Sing s ->
    a ->
    m i ('BoundTexture s ': Delete ('BoundTexture s) i) ()
  activateTexture :: TextureUnit -> m i i ()
  unbindTexture ::
    ( KnownSymbol s,
      GL.BindableTextureTarget a,
      If (Elem ('BoundTexture s) i) 'True (TrackError "Texture" s "is not bound") ~ 'True
    ) =>
    Sing s ->
    a ->
    m i (Delete ('BoundTexture s) i) ()
  textureFilter ::
    ( KnownSymbol s,
      GL.ParameterizedTextureTarget tgt,
      If (Elem ('BoundTexture s) i) 'True (TrackError "Texture" s "is not bound") ~ 'True
    ) =>
    Sing s ->
    tgt ->
    GL.MinificationFilter ->
    GL.MagnificationFilter ->
    m i i ()
  textureWrapMode ::
    ( KnownSymbol s,
      GL.ParameterizedTextureTarget tgt,
      If (Elem ('BoundTexture s) i) 'True (TrackError "Texture" s "is not bound") ~ 'True
    ) =>
    Sing s ->
    tgt ->
    GL.TextureCoordName ->
    GL.Repetition ->
    GL.Clamping ->
    m i i ()
  loadTexture2D ::
    ( KnownSymbol s,
      GL.TwoDimensionalTextureTarget tgt,
      GL.ParameterizedTextureTarget tgt,
      If (Elem ('BoundTexture s) i) 'True (TrackError "Texture" s "is not bound") ~ 'True,
      Storable (PixelBaseComponent a)
    ) =>
    Sing s ->
    tgt ->
    GL.Proxy ->
    GL.Level ->
    GL.PixelInternalFormat ->
    GL.PixelFormat ->
    GL.Border ->
    Image a ->
    m i i ()
  generateMipMap :: GL.ParameterizedTextureTarget tgt => tgt -> m i i ()

instance (HasGLState st, MonadState st (t IO), MonadIO (t IO)) => TrackedTexture (TrackT (t IO)) where
  createTexture t = TrackT $ do
    tex <- GL.genObjectName
    modify $ over textures $ insert (symbolVal t) tex
  bindTexture t tgt = TrackT $ do
    st <- get
    let tex = M.lookup (symbolVal t) (view textures st)
    GL.textureBinding tgt $= tex
  activateTexture unit = TrackT $ GL.activeTexture $= GL.TextureUnit unit
  unbindTexture _ tgt = TrackT $ GL.textureBinding tgt $= Nothing
  textureFilter _ tgt minf maxf = TrackT $ GL.textureFilter tgt $= (minf, maxf)
  textureWrapMode _ tgt coord rep clamp = TrackT $ GL.textureWrapMode tgt coord $= (rep, clamp)
  loadTexture2D _ tgt p l ifmt fmt b (Image w h d) =
    TrackT $ do
      let sz = GL.TextureSize2D (fromIntegral w) (fromIntegral h)
      liftIO $ unsafeWith d $ \ptr -> GL.texImage2D tgt p l ifmt sz b $ GL.PixelData fmt GL.UnsignedByte ptr
  generateMipMap tgt = TrackT $ GL.generateMipmap tgt $= GL.Enabled
