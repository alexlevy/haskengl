module Graphics.HaskenGL.SafeArrays
  ( createArrayObject,
    bindArrayObject,
    bindElementArray,
    allocateElementArray,
    attribPointer,
    attribPointerFloat,
    enableAttribPointer,
    unbindElementArray,
    unbindArrayObject,
  )
where

import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Indexed
  ( IxMonad (..),
  )
import Control.Monad.State (MonadState, get, modify)
import Data.Bool.Singletons (If)
import Data.Foldable.Singletons (Elem)
import Data.List.Singletons (Delete)
import Data.Map.Strict (insert)
import Data.Map.Strict qualified as M
import Data.Singletons (Sing)
import Foreign (Storable)
import Foreign.Storable (sizeOf)
import GHC.Ptr (nullPtr, plusPtr)
import GHC.TypeLits (natVal, symbolVal)
import GHC.TypeLits.Singletons (KnownNat, KnownSymbol)
import Graphics.HaskenGL.Internal.Types
  ( HasGLState,
    KnownVertex,
    SizedVertex (SizedVertex),
    TrackedObject
      ( AttribPointer,
        BoundVertexArrayObject,
        ElementArrayBuffer,
        VertexArrayObject
      ),
    vaos,
  )
import Graphics.HaskenGL.SafeBuffers (allocateBuffer)
import Graphics.HaskenGL.Tracker (TrackError, TrackT (TrackT))
import Graphics.Rendering.OpenGL qualified as GL
import Graphics.Rendering.OpenGL.GL (($=))
import Lens.Micro (over)
import Lens.Micro.Extras (view)

class (IxMonad m) => TrackedElementArray m where
  bindElementArray ::
    ( KnownSymbol s,
      KnownSymbol v,
      If (Elem ('ElementArrayBuffer s) i) (TrackError "EBO" s "already exists") 'False ~ 'False,
      If (Elem ('BoundVertexArrayObject v) i) 'True (TrackError "VAO" v "is not bound") ~ 'True
    ) =>
    Sing s ->
    Sing v ->
    m i ('ElementArrayBuffer s ': Delete ('ElementArrayBuffer s) i) ()
  allocateElementArray ::
    ( KnownSymbol s,
      KnownSymbol v,
      KnownVertex n a,
      If (Elem ('ElementArrayBuffer s) i) 'True (TrackError "EBO" s "does not exist") ~ 'True,
      If (Elem ('BoundVertexArrayObject v) i) 'True (TrackError "VAO" v "is not bound") ~ 'True
    ) =>
    Sing s ->
    Sing v ->
    GL.BufferUsage ->
    [a] ->
    m i i ()
  unbindElementArray ::
    ( KnownSymbol s,
      KnownSymbol v,
      If (Elem ('BoundVertexArrayObject v) i) (TrackError "VAO" v "is still bound") 'False ~ 'False,
      If (Elem ('ElementArrayBuffer s) i) 'True (TrackError "EBO" s "does not exist") ~ 'True
    ) =>
    Sing s ->
    Sing v ->
    m i (Delete ('ElementArrayBuffer s) i) ()

class (IxMonad m) => TrackedArrayObject m where
  createArrayObject ::
    ( KnownSymbol s,
      If (Elem ('VertexArrayObject s) i) (TrackError "VAO" s "already exists") 'False ~ 'False
    ) =>
    Sing s ->
    m i ('VertexArrayObject s ': i) ()
  bindArrayObject ::
    ( KnownSymbol s,
      If (Elem ('VertexArrayObject s) i) 'True (TrackError "VAO" s "does not exist") ~ 'True,
      If (Elem ('BoundVertexArrayObject s) i) (TrackError "VAO" s "is already bound") 'False ~ 'False
    ) =>
    Sing s ->
    m i ('BoundVertexArrayObject s ': Delete ('BoundVertexArrayObject s) i) ()
  unbindArrayObject ::
    ( KnownSymbol s,
      If (Elem ('BoundVertexArrayObject s) i) 'True (TrackError "VAO" s "is not bound") ~ 'True
    ) =>
    Sing s ->
    m i (Delete ('BoundVertexArrayObject s) i) ()

class (IxMonad m) => TrackedAttribPointer m where
  attribPointer ::
    ( KnownNat n,
      KnownSymbol v,
      If (Elem ('BoundVertexArrayObject v) i) 'True (TrackError "VAO" v "is not bound") ~ 'True,
      Storable a
    ) =>
    Sing n ->
    Sing v ->
    a ->
    GL.DataType ->
    GL.IntegerHandling ->
    GL.NumComponents ->
    Maybe GL.Stride ->
    Maybe GL.Offset ->
    m i ('AttribPointer n ': i) ()
  enableAttribPointer ::
    ( KnownNat n,
      KnownSymbol v,
      If (Elem ('BoundVertexArrayObject v) i) 'True (TrackError "VAO" v "is not bound") ~ 'True,
      If (Elem ('AttribPointer n) i) 'True (TrackError "Attrib pointer" n "does not exist") ~ 'True
    ) =>
    Sing n ->
    Sing v ->
    m i (Delete ('AttribPointer n) i) ()

instance (HasGLState st, MonadIO m, MonadState st m) => TrackedArrayObject (TrackT m) where
  createArrayObject v = TrackT $ do
    vao <- GL.genObjectName
    modify $ over vaos $ insert (symbolVal v) vao
  bindArrayObject v = TrackT $ do
    st <- get
    GL.bindVertexArrayObject $= M.lookup (symbolVal v) (view vaos st)
  unbindArrayObject _ = TrackT $ GL.bindVertexArrayObject $= Nothing

instance (MonadState st (t IO), MonadIO (t IO)) => TrackedElementArray (TrackT (t IO)) where
  bindElementArray _ _ = TrackT $ do
    ebo <- GL.genObjectName
    GL.bindBuffer GL.ElementArrayBuffer $= Just ebo
  unbindElementArray _ _ = TrackT $ GL.bindBuffer GL.ElementArrayBuffer $= Nothing
  allocateElementArray _ _ u = TrackT . liftIO . allocateBuffer GL.ElementArrayBuffer u . fmap SizedVertex

instance (MonadIO m, MonadState st m) => TrackedAttribPointer (TrackT m) where
  attribPointer n _ re dt ih nc ms mo = TrackT $ do
    let loc = GL.AttribLocation $ fromIntegral $ natVal n
    let s = maybe 0 (* fromIntegral (sizeOf re)) ms
    let o = maybe nullPtr (\o' -> plusPtr nullPtr $ fromIntegral o' * sizeOf re) mo
    let d = GL.VertexArrayDescriptor nc dt s o
    GL.vertexAttribPointer loc $= (ih, d)
  enableAttribPointer n _ =
    let loc = GL.AttribLocation $ fromIntegral (natVal n)
     in TrackT $ GL.vertexAttribArray loc $= GL.Enabled

attribPointerFloat ::
  ( TrackedAttribPointer m,
    KnownNat n,
    KnownSymbol v,
    If (Elem ('BoundVertexArrayObject v) i) 'True (TrackError "VAO" v "is not bound") ~ 'True
  ) =>
  Sing n ->
  Sing v ->
  GL.NumComponents ->
  Maybe GL.Stride ->
  Maybe GL.Offset ->
  m i ('AttribPointer n : i) ()
attribPointerFloat n v = attribPointer n v (undefined :: GL.GLfloat) GL.Float GL.ToFloat
