module Graphics.HaskenGL.Shader
  ( createProgram,
    loadShaderSource,
  )
where

import Control.Monad (forM, forM_, unless)
import Data.ByteString as BS (ByteString, readFile)
import Graphics.HaskenGL.Internal.Types (Shader (Shader))
import Graphics.Rendering.OpenGL (($=))
import Graphics.Rendering.OpenGL qualified as GL
import Prelude hiding (log)

compileShader :: Shader -> IO GL.Shader
compileShader (Shader t fp) = do
  shader <- GL.createShader t
  s <- loadShaderSource fp
  GL.shaderSourceBS shader $= s
  monitor GL.compileShader GL.compileStatus GL.shaderInfoLog (show t <> " compile") shader
  pure shader

linkProgram :: (Traversable w) => w GL.Shader -> IO GL.Program
linkProgram shaders = do
  prog <- GL.createProgram
  forM_ shaders $ GL.attachShader prog
  monitor GL.linkProgram GL.linkStatus GL.programInfoLog "Program link" prog
  forM_ shaders GL.deleteObjectName
  pure prog

createProgram :: (Traversable w) => w Shader -> IO GL.Program
createProgram shaders = forM shaders compileShader >>= linkProgram

loadShaderSource :: FilePath -> IO ByteString
loadShaderSource = BS.readFile

monitor ::
  (a -> IO ()) ->
  (a -> GL.GettableStateVar Bool) ->
  (a -> GL.GettableStateVar String) ->
  String ->
  a ->
  IO ()
monitor action status log msg a = do
  action a
  ok <- GL.get $ status a
  unless ok $ do
    infoLog <- GL.get $ log a
    fail $ msg <> " failed due to : " <> infoLog
