{-# LANGUAGE FunctionalDependencies #-}

module Graphics.HaskenGL.Internal.Types
  ( Stride,
    Offset,
    ElementSize,
    TrackedObject (..),
    KnownVertex (..),
    SizedVertex (..),
    EulerAngles (..),
    CursorPosition (..),
    Shader (..),
    TextureUnit,
    GLMajorVersion,
    GLMinorVersion,
    WindowHeight,
    WindowWidth,
    WindowTitle,
    HasGLState (..),
    BindingPoint,
    Camera (..),
    Sensitivity,
  )
where

import Control.Concurrent.STM (TVar)
import Data.Eq.Singletons (DefaultEq, PEq ((==)))
import Data.Map.Strict (Map)
import Foreign (Storable)
import GHC.Int (Int32)
import GHC.TypeLits (KnownNat, Nat)
import GHC.TypeLits.Singletons (Symbol)
import Graphics.Rendering.OpenGL qualified as GL
import Graphics.Rendering.OpenGL.GL (GLuint)
import Lens.Micro (Lens')
import Linear (V3)

data TrackedObject
  = VertexArrayObject Symbol
  | BoundVertexArrayObject Symbol
  | VertexBufferObject Symbol
  | ElementArrayBuffer Symbol
  | AttribPointer Nat
  | Program Symbol
  | UsedProgram Symbol
  | Texture Symbol
  | BoundTexture Symbol
  | BoundUniformBlock Symbol Symbol
  | UniformBuffer Symbol
  deriving (Eq)

instance PEq TrackedObject where
  type x == y = DefaultEq x y

type Stride = Int

type Offset = Int

type ElementSize = Int32

type TextureUnit = GLuint

type BindingPoint = GLuint

class HasGLState a where
  vaos :: Lens' a (Map String GL.VertexArrayObject)
  programs :: Lens' a (Map String GL.Program)
  textures :: Lens' a (Map String GL.TextureObject)
  frameDelta :: Lens' a (TVar Double)
  lastFrame :: Lens' a (TVar Double)
  framesByTime :: Lens' a (TVar (Int, Double))
  uniformBuffers :: Lens' a (Map String GL.BufferObject)
  newGLState :: IO a

newtype SizedVertex (n :: Nat) a = SizedVertex a

class (KnownNat n, Storable a) => KnownVertex n a | a -> n where
  getVertex :: SizedVertex n a -> a

instance (Storable a) => KnownVertex 3 (GL.Vertex3 a) where
  getVertex (SizedVertex a) = a

instance (Storable a) => KnownVertex 2 (GL.Vertex2 a) where
  getVertex (SizedVertex a) = a

instance (Storable a) => KnownVertex 1 (GL.Vertex1 a) where
  getVertex (SizedVertex a) = a

instance KnownVertex 1 GL.GLuint where
  getVertex (SizedVertex a) = a

data EulerAngles = EulerAngles {pitch :: !Double, yaw :: !Double}

data CursorPosition = CursorPosition {cursorX :: !Double, cursorY :: !Double}

data Shader = Shader !GL.ShaderType !FilePath

type GLMajorVersion = Int

type GLMinorVersion = Int

type WindowHeight = Int

type WindowWidth = Int

type WindowTitle = String

data Camera = Camera
  { cameraPos :: !(V3 GL.GLfloat),
    cameraFront :: !(V3 GL.GLfloat),
    cameraUp :: !(V3 GL.GLfloat)
  }
  deriving (Show)

type Sensitivity = Double
