{-# LANGUAGE RecordWildCards #-}

module Graphics.HaskenGL.Camera (flyModeCamera) where

import Control.Concurrent.STM (TVar, atomically, modifyTVar, readTVar)
import Data.Fixed (mod')
import Data.Set qualified as S
import GHC.Float (double2Float)
import Graphics.HaskenGL.Internal.Types (Camera (..), EulerAngles (EulerAngles), Sensitivity)
import Graphics.Rendering.OpenGL.GL qualified as GL
import Graphics.UI.GLFW qualified as GLFW
import Linear (V3 (V3), cross, signorm, (*^), (^+^), (^-^))

flyModeCamera :: TVar Camera -> Sensitivity -> S.Set GLFW.Key -> EulerAngles -> IO Camera
flyModeCamera cam speed keys euler = atomically $ do
  cam' <- readTVar cam
  let pos = flyModeCameraPosition cam' keys
  let front = flyModeCameraOrientation euler
  modifyTVar cam (\c@Camera {..} -> c {cameraFront = front, cameraPos = cameraPos ^+^ (double2Float speed *^ pos)})
  readTVar cam

flyModeCameraOrientation :: EulerAngles -> V3 GL.GLfloat
flyModeCameraOrientation (EulerAngles pitch yaw) =
  let yawRad = pi / 180 * mod' yaw 360
      pitchRad = pi / 180 * mod' pitch 360
      x = cos yawRad * cos pitchRad
      y = sin pitchRad
      z = sin yawRad * cos pitchRad
   in realToFrac <$> signorm (V3 x y z)

flyModeCameraPosition :: Camera -> S.Set GLFW.Key -> V3 GL.GLfloat
flyModeCameraPosition Camera {..} =
  foldr
    ( \key vec -> case key of
        GLFW.Key'W -> vec ^+^ cameraFront
        GLFW.Key'S -> vec ^-^ cameraFront
        GLFW.Key'A -> vec ^-^ signorm (cross cameraFront cameraUp)
        GLFW.Key'D -> vec ^+^ signorm (cross cameraFront cameraUp)
        _ -> vec
    )
    (V3 0 0 0)
