module Graphics.HaskenGL.Image
  ( loadJpeg,
    loadPng,
  )
where

import Codec.Picture (DynamicImage (ImageRGBA8, ImageYCbCr8), Image (Image), PixelRGB8, PixelRGBA8, decodeJpeg, generateImage, pixelAt)
import Codec.Picture.Png (decodePng)
import Codec.Picture.Types (ColorSpaceConvertible (convertImage))
import qualified Data.ByteString as BS (readFile)

loadJpeg :: FilePath -> IO (Image PixelRGB8)
loadJpeg path = do
  raw <- BS.readFile path
  let decoded = either error id $ decodeJpeg raw
  case decoded of
    ImageYCbCr8 img -> pure $ convertImage img
    _ -> error $ "Invalid JPEG image " <> path

loadPng :: FilePath -> IO (Image PixelRGBA8)
loadPng path = do
  raw <- BS.readFile path
  let decoded = either error id $ decodePng raw
  case decoded of
    ImageRGBA8 img@(Image w h _) -> pure $ generateImage (\x y -> pixelAt img x (h - 1 - y)) w h
    _ -> error $ "Invalid PNG image " <> path
