module Graphics.HaskenGL.SafeBuffers
  ( allocateBuffer,
    bindBufferObject,
    allocateBufferObject,
    unbindBufferObject,
  )
where

import Control.Monad.IO.Class (MonadIO (liftIO))
import Control.Monad.Indexed (IxMonad)
import Control.Monad.State (MonadState)
import Data.Bool.Singletons (If)
import Data.Data (Proxy (..))
import Data.Foldable.Singletons (Elem)
import Data.List.Singletons (Delete)
import Data.Singletons (Sing)
import Foreign.Marshal.Array (withArray)
import Foreign.Storable (sizeOf)
import GHC.TypeLits (natVal)
import GHC.TypeLits.Singletons (KnownSymbol)
import Graphics.HaskenGL.Internal.Types
  ( KnownVertex (getVertex),
    SizedVertex (SizedVertex),
    TrackedObject (BoundVertexArrayObject, VertexBufferObject),
  )
import Graphics.HaskenGL.Tracker (TrackError, TrackT (TrackT))
import Graphics.Rendering.OpenGL (($=))
import Graphics.Rendering.OpenGL qualified as GL

class IxMonad m => TrackedBufferObject m where
  bindBufferObject ::
    ( KnownSymbol s,
      KnownSymbol v,
      If (Elem ('VertexBufferObject s) i) (TrackError "VBO" s "already exists") 'False ~ 'False,
      If (Elem ('BoundVertexArrayObject v) i) 'True (TrackError "VAO" v "is not bound") ~ 'True
    ) =>
    Sing s ->
    Sing v ->
    m i ('VertexBufferObject s ': Delete ('VertexBufferObject s) i) ()
  allocateBufferObject ::
    ( KnownSymbol s,
      KnownSymbol v,
      KnownVertex n a,
      If (Elem ('VertexBufferObject s) i) 'True (TrackError "VBO" s "does not exist") ~ 'True,
      If (Elem ('BoundVertexArrayObject v) i) 'True (TrackError "VAO" v "is not bound") ~ 'True
    ) =>
    Sing s ->
    Sing v ->
    GL.BufferUsage ->
    [a] ->
    m i i ()
  unbindBufferObject ::
    ( KnownSymbol s,
      If (Elem ('VertexBufferObject s) i) 'True (TrackError "VBO" s "does not exist") ~ 'True
    ) =>
    Sing s ->
    m i (Delete ('VertexBufferObject s) i) ()

instance (MonadState st (t IO), MonadIO (t IO)) => TrackedBufferObject (TrackT (t IO)) where
  bindBufferObject _ _ = TrackT $ do
    vbo <- GL.genObjectName
    GL.bindBuffer GL.ArrayBuffer $= Just vbo
  unbindBufferObject _ = TrackT $ GL.bindBuffer GL.ArrayBuffer $= Nothing
  allocateBufferObject _ _ u = TrackT . liftIO . allocateBuffer GL.ArrayBuffer u . fmap SizedVertex

allocateBuffer :: forall a n. KnownVertex n a => GL.BufferTarget -> GL.BufferUsage -> [SizedVertex n a] -> IO ()
allocateBuffer target usage array = do
  let vertexSize = natVal (Proxy :: Proxy n)
  withArray (getVertex <$> array) $ \ptr -> do
    let size = fromIntegral $ length array * fromIntegral vertexSize * sizeOf (undefined :: a)
    GL.bufferData target GL.$= (size, ptr, usage)
