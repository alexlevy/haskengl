# The job
Builds a source distribution that can further be uploaded to [Hackage](https://hackage.haskell.org/). A job to upload to hackage is in the works 💪

# How to use

1. Include the job
   ```yaml
   include:
     - "/jobs/stack_build/stack_sdist.yml"
   ```
2. Add `HASKELL_VERSION` variable which is used as the tag of the Haskell Docker image to use (see https://hub.docker.com/_/haskell).
   ```yaml
   variables:
     HASKELL_VERSION: 8.10.7
   ```

# Variables
| Name | Description | Default |
| ---- | ----------- | ------- |
| `HASKELL_VERSION` | The tag (i.e Haskell version) of the Haskell docker image to use | Mandatory, no default as it is driven by the LTS snapshot you use for your project |
| `PROJECT_VERSION` | The version of your project | Parsed `version` value from `package.yaml` |
| `ARTIFACT_DIR` | The folder where to create the tar archive |
