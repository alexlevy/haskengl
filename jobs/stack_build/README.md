# The job

Builds a Haskell project using stack and create a source distribution. The job caches the dependencies so that they are not recompiled unless either `stack.yaml.lock` or `package.yaml` change.
This drastically reduces the build time for subsequent pipelines. Haskell is great but GHC likes to take its time to compile, just grab a ☕ and relax for the first pipeline :)

# How to use

1. Include the job
   ```yaml
   include:
     - "/jobs/stack_build/stack_build.yml"
   ```
2. Add `HASKELL_VERSION` variable which is used as the tag of the Haskell Docker image to use (see https://hub.docker.com/_/haskell).
   ```yaml
   variables:
     HASKELL_VERSION: 8.10.7
   ```

# Variables
| Name | Description | Default |
| ---- | ----------- | ------- |
| `HASKELL_VERSION` | The tag (i.e Haskell version) of the Haskell docker image to use | Mandatory, no default as it is driven by the LTS snapshot you use for your project |
