# The job

Builds a Haskell project using Cabal. The job caches the dependencies so that they are not recompiled unless `cabal.project.freeze` and/or your project cabal file change. The file `cabal.project.freeze`
is generated after building so you don't even have to take care of it.

Yet another way to build your awesome project with Haskell. Please refer to the [stack_build](https://r2devops.io/_/alexlevy/stack_build) job if you prefer to build with Stack rather than with Cabal.

Stack actually leverages Cabal behind the scenes to build you project, it's essentially a wrapper with some interesting features. Cabal is arguably "closer to vanilla" Haskell, but choosing between
Stack and Cabal mostly remains a matter of taste.

[Here](https://cabal.readthedocs.io/en/3.6/getting-started.html) is a great place to get started with Cabal.

# How to use

1. Include the job
   ```yaml
   include:
     - "/jobs/cabal_build/cabal_build.yml"
   ```
2. Add `HASKELL_VERSION` variable which is used as the tag of the [Haskell Docker image](https://hub.docker.com/_/haskell).
   ```yaml
   variables:
     HASKELL_VERSION: 8.10.7
   ```

# Variables
| Name | Description | Default |
| ---- | ----------- | ------- |
| `HASKELL_VERSION` | The tag (i.e Haskell version) of the Haskell docker image to use | Mandatory, no default as it is driven by the LTS snapshot you use for your project |
| `CABAL_PROJECT_NAME` | The name of your Cabal project | `${CI_PROJECT_NAME}` |
| `CABAL_ROOT_DIR` | The root directory for Cabal | `${CI_PROJECT_DIR}/cabal` |
| `CABAL_STORE_DIR` | The directory of the global package store | `${CABAL_ROOT_DIR}/store` |
| `CABAL_REMOTE_REPO_DIR` | The location where packages downloaded from remote repositories will be cached | `${CABAL_ROOT_DIR}/packages` |
