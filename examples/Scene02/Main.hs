{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import Control.Concurrent.STM (TVar, newTVarIO)
import Control.Monad (forM_, unless)
import Control.Monad.State (StateT)
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Foreign (Ptr)
import Foreign.Marshal (new)
import GHC.TypeLits.Singletons
import Graphics.HaskenGL
import qualified Graphics.Rendering.OpenGL as GL
import qualified Graphics.UI.GLFW as GLFW
import Lens.Micro.TH (makeLenses)
import Linear (axisAngle, lookAt, perspective)
import Linear.Matrix (M44, (!*!))
import Linear.V3 (V3 (..))

data SceneState = SceneState
  { _sceneVAOs :: Map String GL.VertexArrayObject,
    _scenePrograms :: Map String GL.Program,
    _sceneTextures :: Map String GL.TextureObject,
    _sceneUniformBuffers :: Map String GL.BufferObject,
    _sceneFrameDelta :: TVar Double,
    _sceneLastFrame :: TVar Double,
    _sceneFramesByTime :: TVar (Int, Double)
  }

$(makeLenses ''SceneState)

instance HasGLState SceneState where
  vaos = sceneVAOs
  programs = scenePrograms
  textures = sceneTextures
  lastFrame = sceneLastFrame
  frameDelta = sceneFrameDelta
  framesByTime = sceneFramesByTime
  uniformBuffers = sceneUniformBuffers
  newGLState = do
    fd <- newTVarIO 0
    lf <- newTVarIO 0
    fbt <- newTVarIO (0, 0)
    pure $ SceneState M.empty M.empty M.empty M.empty fd lf fbt

main :: IO ()
main = withWindow 4 3 1024 768 "HaskenGL Scene 02" $ \win ->
  track $
    (◆) setupViewProjectionUniform
      ∙ setupLightsUniform
      ∙ createFloorProgram
      ∙ createWallProgram
      ∙ run win (capFps 30 drawScene)
      ∙ tearDown

drawFloor :: TrackT (StateT SceneState IO) _ _ ()
drawFloor =
  do
    useProgram (SSym @"floor")
    ∙ activateTexture 0
    ∙ bindTexture (SSym @"woodFloorColor") GL.Texture2D
    ∙ bindArrayObject (SSym @"floorVAO")
    ∙ (↑!) (glMatrix (translate (V3 (0 :: GL.GLfloat) 0 0) !*! rotate (axisAngle (V3 0 1 0) 0)))
    ▶ setUniform (SSym @"floor") "model"
    ∙ drawElements (SSym @"floor") (SSym @"floorVAO") GL.Triangles 6 GL.UnsignedInt
    ∙ (↑!) (glMatrix (translate (V3 (0 :: GL.GLfloat) 0 (-1)) !*! rotate (axisAngle (V3 0 1 0) 0)))
    ▶ setUniform (SSym @"floor") "model"
    ∙ drawElements (SSym @"floor") (SSym @"floorVAO") GL.Triangles 6 GL.UnsignedInt
    ∙ unbindArrayObject (SSym @"floorVAO")
    ∙ unuseProgram (SSym @"floor")
    ∙ unbindTexture (SSym @"woodFloorColor") GL.Texture2D

drawWall :: V3 GL.GLfloat -> GL.GLfloat -> TrackT (StateT SceneState IO) _ _ ()
drawWall trans angle = do
  mat <- (↑!) $ glMatrix (translate trans !*! rotate (axisAngle (V3 0 1 0) angle))
  setUniform (SSym @"wall") "model" mat
  drawElements (SSym @"wall") (SSym @"wallVAO") GL.Triangles 6 GL.UnsignedInt

drawWalls :: TrackT (StateT SceneState IO) _ _ ()
drawWalls =
  do
    useProgram (SSym @"wall")
    ∙ activateTexture 0
    ∙ bindTexture (SSym @"brickWallColor") GL.Texture2D
    ∙ bindArrayObject (SSym @"wallVAO")
    ∙ forM_ [(V3 1 0 0, -pi / 2), (V3 1 0 (-1), -pi / 2), (V3 0 0 (-2), 0), (V3 (-1) 0 0, pi / 2), (V3 (-1) 0 (-1), pi / 2)] (uncurry drawWall)
    ∙ unbindArrayObject (SSym @"wallVAO")
    ∙ unuseProgram (SSym @"wall")
    ∙ unbindTexture (SSym @"brickWallColor") GL.Texture2D

drawScene :: TrackT (StateT SceneState IO) _ _ ()
drawScene = drawFloor ∙ drawWalls

viewProjectionMatrix :: IO (Ptr (M44 GL.GLfloat))
viewProjectionMatrix = new $ perspective @GL.GLfloat (pi / 4) (4 / 3) 0.1 100 !*! lookAt @GL.GLfloat (V3 0 0.7 1.74) (V3 (-0.208) 0 (-0.36)) (V3 0 1 0)

setupViewProjectionUniform :: TrackT (StateT SceneState IO) '[] _ ()
setupViewProjectionUniform =
  do
    createUniformBuffer (SSym @"vpUBO") GL.StaticDraw 64
    ∙ (↑!) viewProjectionMatrix
    ▶ fillUniformBuffer (SSym @"vpUBO") 0 64

setupLightsUniform :: TrackT (StateT SceneState IO) _ _ ()
setupLightsUniform =
  do
    createUniformBuffer (SSym @"lightsUBO") GL.StaticDraw 188
    ∙ (↑!) (new $ V3 (0.2 :: GL.GLfloat) 0.2 0.2)
    ▶ fillUniformBuffer (SSym @"lightsUBO") 0 12 -- ambient
    ∙ directionalLight
    ∙ pointLight1
    ∙ pointLight2

directionalLight :: TrackT (StateT SceneState IO) _ _ ()
directionalLight =
  do
    (↑!) (new $ V3 (0.5 :: GL.GLfloat) (-0.8) (-0.5))
    ▶ fillUniformBuffer (SSym @"lightsUBO") 16 12 -- direction
    ∙ (↑!) (new $ V3 (1 :: GL.GLfloat) 0.99 0.95)
    ▶ fillUniformBuffer (SSym @"lightsUBO") 32 12 -- diffuse
    ∙ (↑!) (new $ V3 (1 :: GL.GLfloat) 1 1)
    ▶ fillUniformBuffer (SSym @"lightsUBO") 48 12 -- specular
    ∙ (↑!) (new (0.7 :: GL.GLfloat))
    ▶ fillUniformBuffer (SSym @"lightsUBO") 60 4 -- strength

pointLight1 :: TrackT (StateT SceneState IO) _ _ ()
pointLight1 =
  do
    (↑!) (new $ V3 (-0.7 :: GL.GLfloat) 0.7 (-0.65))
    ▶ fillUniformBuffer (SSym @"lightsUBO") 64 12 -- position
    ∙ (↑!) (new $ V3 (0.8 :: GL.GLfloat) 0.8 0.8)
    ▶ fillUniformBuffer (SSym @"lightsUBO") 80 12 -- diffuse
    ∙ (↑!) (new $ V3 (1 :: GL.GLfloat) 1 1)
    ▶ fillUniformBuffer (SSym @"lightsUBO") 96 12 -- specular
    ∙ (↑!) (new (1 :: GL.GLfloat))
    ▶ fillUniformBuffer (SSym @"lightsUBO") 108 4 -- constant attenuation
    ∙ (↑!) (new (0.22 :: GL.GLfloat))
    ▶ fillUniformBuffer (SSym @"lightsUBO") 112 4 -- linear attenuation
    ∙ (↑!) (new (0.2 :: GL.GLfloat))
    ▶ fillUniformBuffer (SSym @"lightsUBO") 116 4 -- quadratic attenuation
    ∙ (↑!) (new (0.8 :: GL.GLfloat))
    ▶ fillUniformBuffer (SSym @"lightsUBO") 120 4 -- strength

pointLight2 :: TrackT (StateT SceneState IO) _ _ ()
pointLight2 =
  do
    (↑!) (new $ V3 (0.75 :: GL.GLfloat) 0.4 (-0.8))
    ▶ fillUniformBuffer (SSym @"lightsUBO") 128 12 -- position
    ∙ (↑!) (new $ V3 (0.8 :: GL.GLfloat) 0.8 0.8)
    ▶ fillUniformBuffer (SSym @"lightsUBO") 144 12 -- diffuse
    ∙ (↑!) (new $ V3 (1 :: GL.GLfloat) 1 1)
    ▶ fillUniformBuffer (SSym @"lightsUBO") 160 12 -- specular
    ∙ (↑!) (new (1 :: GL.GLfloat))
    ▶ fillUniformBuffer (SSym @"lightsUBO") 172 4 -- constant attenuation
    ∙ (↑!) (new (0.7 :: GL.GLfloat))
    ▶ fillUniformBuffer (SSym @"lightsUBO") 176 4 -- linear attenuation
    ∙ (↑!) (new (1.8 :: GL.GLfloat))
    ▶ fillUniformBuffer (SSym @"lightsUBO") 180 4 -- quadratic attenuation
    ∙ (↑!) (new (0.7 :: GL.GLfloat))
    ▶ fillUniformBuffer (SSym @"lightsUBO") 184 4 -- strength

createFloorProgram :: TrackT (StateT SceneState IO) _ _ ()
createFloorProgram =
  do
    let vertexShader = Shader GL.VertexShader "examples/Scene02/shaders/floor_vert.glsl"
    let fragmentShader = Shader GL.FragmentShader "examples/Scene02/shaders/floor_frag.glsl"

    createProgram (SSym @"floor") [vertexShader, fragmentShader]
    ∙ loadFloorTextures
    ∙ useProgram (SSym @"floor")
    ∙ createArrayObject (SSym @"floorVAO")
    ∙ bindArrayObject (SSym @"floorVAO")
    ∙ bindBufferObject (SSym @"floorVBO") (SSym @"floorVAO")
    ∙ allocateBufferObject
      (SSym @"floorVBO")
      (SSym @"floorVAO")
      GL.StaticDraw
      [ GL.Vertex3 @GL.GLfloat (-1) 0 1, -- back left
        GL.Vertex3 1 0 1, -- back right
        GL.Vertex3 1 0 (-1), -- front right
        GL.Vertex3 (-1) 0 (-1) -- front left
      ]
    ∙ attribPointerFloat (SNat @0) (SSym @"floorVAO") 3 (Just 3) Nothing
    ∙ enableAttribPointer (SNat @0) (SSym @"floorVAO")
    ∙ bindElementArray (SSym @"floorEBO") (SSym @"floorVAO")
    ∙ allocateElementArray
      (SSym @"floorEBO")
      (SSym @"floorVAO")
      GL.StaticDraw
      [0 :: GL.GLuint, 1, 2, 2, 3, 0]
    ∙ unbindBufferObject (SSym @"floorVBO")
    ∙ bindBufferObject (SSym @"floorTexCoords") (SSym @"floorVAO")
    ∙ allocateBufferObject
      (SSym @"floorTexCoords")
      (SSym @"floorVAO")
      GL.StaticDraw
      [ GL.Vertex2 @GL.GLfloat 0 0,
        GL.Vertex2 1.7 0,
        GL.Vertex2 1.7 1.7,
        GL.Vertex2 0 1.7
      ]
    ∙ attribPointerFloat (SNat @1) (SSym @"floorVAO") 2 (Just 2) Nothing
    ∙ enableAttribPointer (SNat @1) (SSym @"floorVAO")
    ∙ unbindBufferObject (SSym @"floorTexCoords")
    ∙ unbindArrayObject (SSym @"floorVAO")
    ∙ unbindElementArray (SSym @"floorEBO") (SSym @"floorVAO")
    ∙ bindUniformBlock (SSym @"floor") (SSym @"VPMatrix") (SSym @"vpUBO") 0 Nothing
    ∙ bindUniformBlock (SSym @"floor") (SSym @"Lights") (SSym @"lightsUBO") 1 Nothing
    ∙ setUniform (SSym @"floor") "viewPosition" (GL.Vertex3 (0.4 :: GL.GLfloat) 1.25 2)
    ∙ setUniform (SSym @"floor") "woodFloorColor" (GL.TextureUnit 0)
    ∙ unuseProgram (SSym @"floor")

createWallProgram :: TrackT (StateT SceneState IO) _ _ ()
createWallProgram =
  do
    let vertexShader = Shader GL.VertexShader "examples/Scene02/shaders/wall_vert.glsl"
    let fragmentShader = Shader GL.FragmentShader "examples/Scene02/shaders/wall_frag.glsl"

    createProgram (SSym @"wall") [vertexShader, fragmentShader]
    ∙ loadWallTextures
    ∙ useProgram (SSym @"wall")
    ∙ createArrayObject (SSym @"wallVAO")
    ∙ bindArrayObject (SSym @"wallVAO")
    ∙ bindBufferObject (SSym @"wallVBO") (SSym @"wallVAO")
    ∙ allocateBufferObject
      (SSym @"wallVBO")
      (SSym @"wallVAO")
      GL.StaticDraw
      [ GL.Vertex3 @GL.GLfloat (-1) 0 0, -- bottom left
        GL.Vertex3 0 0 1, -- normal
        GL.Vertex3 1 0 0, -- bottom right
        GL.Vertex3 0 0 1, -- normal
        GL.Vertex3 1 1 0, -- up right
        GL.Vertex3 0 0 1, -- normal
        GL.Vertex3 (-1) 1 0, -- up left
        GL.Vertex3 0 0 1 -- normal
      ]
    ∙ bindElementArray (SSym @"wallEBO") (SSym @"wallVAO")
    ∙ allocateElementArray
      (SSym @"wallEBO")
      (SSym @"wallVAO")
      GL.StaticDraw
      [0 :: GL.GLuint, 1, 2, 2, 3, 0]
    ∙ attribPointerFloat (SNat @0) (SSym @"wallVAO") 3 (Just 6) Nothing
    ∙ attribPointerFloat (SNat @1) (SSym @"wallVAO") 3 (Just 6) (Just 3)
    ∙ enableAttribPointer (SNat @0) (SSym @"wallVAO")
    ∙ enableAttribPointer (SNat @1) (SSym @"wallVAO")
    ∙ unbindBufferObject (SSym @"wallVBO")
    ∙ bindBufferObject (SSym @"wallTexCoords") (SSym @"wallVAO")
    ∙ allocateBufferObject
      (SSym @"wallTexCoords")
      (SSym @"wallVAO")
      GL.StaticDraw
      [ GL.Vertex2 @GL.GLfloat 0 0,
        GL.Vertex2 0.7 0,
        GL.Vertex2 0.7 0.7,
        GL.Vertex2 0 0.7
      ]
    ∙ attribPointerFloat (SNat @2) (SSym @"wallVAO") 2 (Just 2) Nothing
    ∙ enableAttribPointer (SNat @2) (SSym @"wallVAO")
    ∙ unbindArrayObject (SSym @"wallVAO")
    ∙ unbindElementArray (SSym @"wallEBO") (SSym @"wallVAO")
    ∙ bindUniformBlock (SSym @"wall") (SSym @"VPMatrix") (SSym @"vpUBO") 0 Nothing
    ∙ bindUniformBlock (SSym @"wall") (SSym @"Lights") (SSym @"lightsUBO") 1 Nothing
    ∙ setUniform (SSym @"wall") "viewPosition" (GL.Vertex3 (0.4 :: GL.GLfloat) 1.25 2)
    ∙ setUniform (SSym @"wall") "brickWallColor" (GL.TextureUnit 0)
    ∙ unuseProgram (SSym @"wall")

loadFloorTextures :: TrackT (StateT SceneState IO) _ _ ()
loadFloorTextures =
  do
    createTexture (SSym @"woodFloorColor")
    ∙ bindTexture (SSym @"woodFloorColor") GL.Texture2D
    ∙ textureFilter (SSym @"woodFloorColor") GL.Texture2D (GL.Linear', Nothing) GL.Linear'
    ∙ textureWrapMode (SSym @"woodFloorColor") GL.Texture2D GL.S GL.Repeated GL.Repeat
    ∙ textureWrapMode (SSym @"woodFloorColor") GL.Texture2D GL.T GL.Repeated GL.Repeat
    ∙ (↑!) (loadJpeg "examples/Scene02/textures/wooden_floor/color.jpg")
    ▶ loadTexture2D (SSym @"woodFloorColor") GL.Texture2D GL.NoProxy 0 GL.RGB' GL.RGB 0
    ∙ unbindTexture (SSym @"woodFloorColor") GL.Texture2D

loadWallTextures :: TrackT (StateT SceneState IO) _ _ ()
loadWallTextures =
  do
    createTexture (SSym @"brickWallColor")
    ∙ bindTexture (SSym @"brickWallColor") GL.Texture2D
    ∙ textureFilter (SSym @"brickWallColor") GL.Texture2D (GL.Linear', Nothing) GL.Linear'
    ∙ textureWrapMode (SSym @"brickWallColor") GL.Texture2D GL.S GL.Repeated GL.Repeat
    ∙ textureWrapMode (SSym @"brickWallColor") GL.Texture2D GL.T GL.Repeated GL.Repeat
    ∙ (↑!) (loadJpeg "examples/Scene02/textures/bricks_wall/color.jpg")
    ▶ loadTexture2D (SSym @"brickWallColor") GL.Texture2D GL.NoProxy 0 GL.RGB' GL.RGB 0
    ∙ unbindTexture (SSym @"brickWallColor") GL.Texture2D

run :: GLFW.Window -> TrackT (StateT SceneState IO) _ _ () -> TrackT (StateT SceneState IO) _ _ ()
run window f = do
  q <- (↑!) $ GLFW.windowShouldClose window
  (↑!) $ GL.clearColor GL.$= GL.Color4 0.1 0.1 0.1 1.0
  (↑!) $ GL.depthFunc GL.$= Just GL.Less
  (↑!) $ GL.clear [GL.ColorBuffer, GL.DepthBuffer]
  f
  (↑!) $ GLFW.swapBuffers window
  (↑!) GLFW.pollEvents
  unless q $ run window f
