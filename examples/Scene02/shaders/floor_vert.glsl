#version 430 core

out vec3 fragPosition;
out vec2 texCoords;

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 inTexCoords;

layout (std140, row_major) uniform VPMatrix {
  mat4 vpMatrix;
};

uniform mat4 model;

void main() {
  fragPosition = position;
  texCoords = inTexCoords;
  gl_Position = vpMatrix * model * vec4(fragPosition, 1.0);
}
