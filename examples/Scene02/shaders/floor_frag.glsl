#version 430 core

in vec3 fragPosition;
in vec2 texCoords;

out vec4 fragColor;

struct DirectionalLight {
  vec3 direction;

  vec3 diffuse;
  vec3 specular;

  float strength;
};

struct PointLight {
  vec3 position;

  vec3 diffuse;
  vec3 specular;

  float constantAttenuation;
  float linearAttenuation;
  float quadraticAttenuation;

  float strength;
};

layout (std140) uniform Lights {
  vec3 ambient;

  DirectionalLight dirLight;

  PointLight pointLights[2];
};

uniform vec3 viewPosition;

uniform sampler2D woodFloorColor;

vec3 calcDirectionalLight(vec3);

vec3 calcPointLight(vec3, PointLight);

void main () {
  vec3 normal = vec3(0.0, 1.0, 0.0); // hardcoded floor normal

  vec3 result;

  for (int i = 0 ; i < 2 ; i++) {
    result += calcPointLight(normal, pointLights[i]);
  }

  result += calcDirectionalLight(normal);

  fragColor = vec4(result * texture(woodFloorColor, texCoords).rgb, 1.0);
}

vec3 calcDirectionalLight(vec3 normal) {
  vec3 direction = normalize(-dirLight.direction);

  float diff = max(dot(normal, direction), 0.0);

  vec3 viewDirection = normalize(viewPosition - fragPosition);

  vec3 reflectDirection = normalize(reflect(-direction, normal));

  float spec = pow(max(dot(viewDirection, reflectDirection), 0.0), 8);

  vec3 diffuse = diff * dirLight.diffuse;

  vec3 specular =  spec * dirLight.specular;

  return dirLight.strength * (ambient + diffuse + specular);
}

vec3 calcPointLight(vec3 normal, PointLight pointLight) {
  vec3 direction = normalize(pointLight.position - fragPosition);

  float diff = max(dot(normal, direction), 0.0);

  vec3 viewDirection = normalize(viewPosition - fragPosition);

  vec3 reflectDirection = normalize(reflect(-direction, normal));

  float spec = pow(max(dot(viewDirection, reflectDirection), 0.0), 8);

  vec3 diffuse = diff * pointLight.diffuse;

  vec3 specular = spec * pointLight.specular;

  float distance = length(pointLight.position - fragPosition);

  float attenuation  = 1.0 / (pointLight.constantAttenuation + pointLight.linearAttenuation * distance + pointLight.quadraticAttenuation * distance * distance);

  return pointLight.strength * attenuation * (ambient + diffuse + specular);
}
