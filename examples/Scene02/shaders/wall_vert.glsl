#version 430 core

out vec3 normal;
out vec3 fragPosition;
out vec2 texCoords;

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 localNormal;
layout (location = 2) in vec2 inTexCoords;

layout (std140, row_major) uniform VPMatrix {
  mat4 vpMatrix;
};

uniform mat4 model;

void main() {
  // normal = mat3(transpose(inverse(model))) * localNormal; // no need for this as we don't apply non-uniform scale
  normal = mat3(model) * localNormal;
  fragPosition = vec3(model * vec4(position, 1.0));
  texCoords = inTexCoords;
  gl_Position = vpMatrix * model * vec4(position, 1.0);
}
