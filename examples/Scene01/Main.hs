{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import Control.Concurrent.STM
  ( TVar,
    modifyTVar,
    readTVar,
    readTVarIO,
  )
import Control.Concurrent.STM.TVar (newTVarIO)
import Control.Monad (unless)
import Control.Monad.STM (atomically)
import Control.Monad.State (StateT, get, modify)
import Data.Functor ((<&>))
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Maybe (fromJust)
import Data.Set (Set)
import qualified Data.Set as S
import Foreign.Marshal.Utils (new)
import Foreign.Ptr (Ptr)
import GHC.Float (double2Float)
import GHC.TypeLits.Singletons
import Graphics.HaskenGL.Camera (flyModeCamera)
import Graphics.HaskenGL.Image (loadJpeg)
import Graphics.HaskenGL.Internal.Types as H
  ( Camera (..),
    CursorPosition (CursorPosition),
    EulerAngles (EulerAngles),
    HasGLState (..),
    Sensitivity,
    Shader (Shader),
  )
import Graphics.HaskenGL.Linear as H
  ( glMatrix,
    rotate,
    scale,
    translate,
  )
import Graphics.HaskenGL.SafeArrays as H
  ( allocateElementArray,
    attribPointerFloat,
    bindArrayObject,
    bindElementArray,
    createArrayObject,
    enableAttribPointer,
    unbindArrayObject,
    unbindElementArray,
  )
import Graphics.HaskenGL.SafeBuffers as H
  ( allocateBufferObject,
    bindBufferObject,
    unbindBufferObject,
  )
import Graphics.HaskenGL.SafePrograms as H
  ( TrackedProgram (drawElements),
    createProgram,
    drawArrays,
    unuseProgram,
    useProgram,
  )
import Graphics.HaskenGL.SafeTextures
  ( TrackedTexture
      ( activateTexture,
        bindTexture,
        createTexture,
        loadTexture2D,
        textureFilter,
        textureWrapMode,
        unbindTexture
      ),
  )
import Graphics.HaskenGL.SafeUniforms as H
  ( bindUniformBlock,
    createUniformBuffer,
    fillUniformBuffer,
    setUniform,
  )
import Graphics.HaskenGL.Tracker as H
  ( TrackT (TrackT),
    tearDown,
    track,
    (↑),
    (↑!),
    (∙),
    (▶),
    (◆),
  )
import Graphics.HaskenGL.Window
  ( keyCallback,
    mouseCallback,
    showFps,
    updateCounters,
    withWindow,
  )
import qualified Graphics.Rendering.OpenGL.GL as GL
import Graphics.UI.GLFW (getTime)
import qualified Graphics.UI.GLFW as GLFW
import Lens.Micro (set)
import Lens.Micro.Extras (view)
import Lens.Micro.TH (makeLenses)
import Linear
  ( V3 (V3),
    axisAngle,
    perspective,
    (!*!),
    (^+^),
    (^-^),
  )
import Linear.Matrix (M44)
import Linear.Projection (lookAt)

data SceneState = SceneState
  { _sceneVAOs :: Map String GL.VertexArrayObject,
    _scenePrograms :: Map String GL.Program,
    _sceneTextures :: Map String GL.TextureObject,
    _sceneUniformBuffers :: Map String GL.BufferObject,
    _sceneDelta :: TVar Double,
    _sceneLastFrame :: TVar Double,
    _sceneFramesByTime :: TVar (Int, Double),
    _pressedKeys :: TVar (Set GLFW.Key),
    _camera :: TVar Camera,
    _eulerAngles :: TVar EulerAngles,
    _cursorPosition :: TVar CursorPosition,
    _mouseSensitivity :: Sensitivity,
    _movementSpeed :: Sensitivity
  }

$(makeLenses ''SceneState)

instance HasGLState SceneState where
  vaos = sceneVAOs
  programs = scenePrograms
  textures = sceneTextures
  lastFrame = sceneLastFrame
  frameDelta = sceneDelta
  framesByTime = sceneFramesByTime
  newGLState = do
    fd <- newTVarIO 0
    pk <- newTVarIO S.empty
    cam <- newTVarIO $ Camera (V3 0 0.5 1) (V3 0 0 (-1)) (V3 0 1 0)
    ea <- newTVarIO $ EulerAngles 0 (-90)
    cursor <- newTVarIO $ CursorPosition 0 0
    lf <- newTVarIO 0
    fbt <- newTVarIO (0, 0)
    pure $ SceneState M.empty M.empty M.empty M.empty fd lf fbt pk cam ea cursor 15 5
  uniformBuffers = sceneUniformBuffers

main :: IO ()
main = do
  textureTrianglePos <- newTVarIO $ H.scale (V3 0.5 0.5 0.5) !*! H.translate (V3 (-0.45) 0.5 (-0.01))
  keys <- newTVarIO S.empty
  lf <- newTVarIO 0
  withWindow 4 3 800 600 "HaskenGL Scene 01" $ \win ->
    track $
      (◆) (initPressedKeys keys)
        ∙ loadData
        ∙ createTextures
        ∙ (↑) (modify $ set sceneLastFrame lf)
        ∙ run win (drawTriangles textureTrianglePos)
        ∙ tearDown

initPressedKeys :: TVar (Set GLFW.Key) -> TrackT (StateT SceneState IO) '[] '[] ()
initPressedKeys ks = TrackT $ modify (set pressedKeys ks)

createTextures :: TrackT (StateT SceneState IO) _ _ ()
createTextures =
  createTexture (SSym @"tex")
    ∙ bindTexture (SSym @"tex") GL.Texture2D
    ∙ textureFilter (SSym @"tex") GL.Texture2D (GL.Linear', Nothing) GL.Linear'
    ∙ textureWrapMode (SSym @"tex") GL.Texture2D GL.S GL.Repeated GL.Repeat
    ∙ textureWrapMode (SSym @"tex") GL.Texture2D GL.T GL.Repeated GL.Repeat
    ∙ (↑!) (loadJpeg "examples/Scene01/textures/container.jpg")
    ▶ loadTexture2D (SSym @"tex") GL.Texture2D GL.NoProxy 0 GL.RGB' GL.RGB 0
    ∙ useProgram (SSym @"texturedTriangle")
    ∙ setUniform (SSym @"texturedTriangle") "container" (GL.TextureUnit 0)
    ∙ unuseProgram (SSym @"texturedTriangle")
    ∙ unbindTexture (SSym @"tex") GL.Texture2D

drawTriangles :: TVar (M44 GL.GLfloat) -> TrackT (StateT SceneState IO) _ _ ()
drawTriangles pos =
  viewMatrix
    ▶ fillUniformBuffer (SSym @"ubo") 0 64
    ∙ useProgram (SSym @"multiColorTriangle")
    ∙ updateMultiColorTrianglePos
    ▶ setUniform (SSym @"multiColorTriangle") "model"
    ∙ bindArrayObject (SSym @"vao")
    ∙ drawArrays (SSym @"multiColorTriangle") (SSym @"vao") GL.Triangles 0 3
    ∙ drawElements (SSym @"multiColorTriangle") (SSym @"vao") GL.Triangles 18 GL.UnsignedInt
    ∙ useProgram (SSym @"singleColorTriangle")
    ∙ drawElements (SSym @"singleColorTriangle") (SSym @"vao") GL.Triangles 18 GL.UnsignedInt
    ∙ useProgram (SSym @"texturedTriangle")
    ∙ activateTexture 0
    ∙ bindTexture (SSym @"tex") GL.Texture2D
    ∙ updateTexturedTrianglePos pos
    ▶ setUniform (SSym @"texturedTriangle") "model"
    ∙ drawElements (SSym @"texturedTriangle") (SSym @"vao") GL.Triangles 18 GL.UnsignedInt
    ∙ unuseProgram (SSym @"texturedTriangle")
    ∙ unuseProgram (SSym @"singleColorTriangle")
    ∙ unuseProgram (SSym @"multiColorTriangle")
    ∙ unbindTexture (SSym @"tex") GL.Texture2D
    ∙ unbindArrayObject (SSym @"vao")

viewMatrix :: TrackT (StateT SceneState IO) _ _ (Ptr (M44 GL.GLfloat))
viewMatrix = do
  st <- (↑) get
  keys <- (↑!) (readTVarIO $ view pressedKeys st)
  ea <- (↑!) (readTVarIO $ view eulerAngles st)
  dt <- (↑!) (readTVarIO $ view frameDelta st)
  cam <- (↑!) $ flyModeCamera (view camera st) (view movementSpeed st * dt) keys ea
  (↑!) $ new $ lookAt (cameraPos cam) (cameraPos cam ^+^ cameraFront cam) (cameraUp cam)

updateTexturedTrianglePos :: TVar (M44 GL.GLfloat) -> TrackT (StateT SceneState IO) _ _ (GL.GLmatrix GL.GLfloat)
updateTexturedTrianglePos pos = do
  st <- (↑) get
  pos' <- (↑!) $
    atomically $ do
      ks <- readTVar (view pressedKeys st)
      d <- readTVar $ view frameDelta st
      let speed = 2
      let inc = double2Float d * speed
      let t =
            foldr
              ( \key vec ->
                  case key of
                    GLFW.Key'Up -> vec ^+^ V3 @GL.GLfloat 0 inc 0
                    GLFW.Key'Down -> vec ^-^ V3 0 inc 0
                    GLFW.Key'Left -> vec ^-^ V3 inc 0 0
                    GLFW.Key'Right -> vec ^+^ V3 inc 0 0
                    _ -> vec
              )
              (V3 0 0 0)
              ks
      modifyTVar pos (H.translate t !*!)
      readTVar pos
  (↑!) $ glMatrix pos'

loadData :: TrackT (StateT SceneState IO) '[] _ ()
loadData =
  do
    let multiColorVertShader = Shader GL.VertexShader "examples/Scene01/shaders/triangle/multicolor_vert.glsl"
    let multiColorFragShader = Shader GL.FragmentShader "examples/Scene01/shaders/triangle/multicolor_frag.glsl"

    let singleColorVertShader = Shader GL.VertexShader "examples/Scene01/shaders/triangle/singlecolor_vert.glsl"
    let singleColorFragShader = Shader GL.FragmentShader "examples/Scene01/shaders/triangle/singlecolor_frag.glsl"

    let texturedVertShader = Shader GL.VertexShader "examples/Scene01/shaders/triangle/textured_vert.glsl"
    let texturedFragShader = Shader GL.FragmentShader "examples/Scene01/shaders/triangle/textured_frag.glsl"

    createProgram (SSym @"multiColorTriangle") [multiColorVertShader, multiColorFragShader]
      ∙ createProgram (SSym @"singleColorTriangle") [singleColorVertShader, singleColorFragShader]
      ∙ createProgram (SSym @"texturedTriangle") [texturedVertShader, texturedFragShader]
      ∙ createUniformBuffer (SSym @"ubo") GL.StaticDraw 128
      ∙ (↑!) (new $ perspective (pi / 4) (800 / 600) 0.1 100 :: IO (Ptr (M44 GL.GLfloat)))
      ▶ fillUniformBuffer (SSym @"ubo") 64 64
      ∙ bindUniformBlock (SSym @"multiColorTriangle") (SSym @"matrices") (SSym @"ubo") 0 Nothing
      ∙ bindUniformBlock (SSym @"singleColorTriangle") (SSym @"matrices") (SSym @"ubo") 0 Nothing
      ∙ bindUniformBlock (SSym @"texturedTriangle") (SSym @"matrices") (SSym @"ubo") 0 Nothing
      ∙ createArrayObject (SSym @"vao")
      ∙ bindArrayObject (SSym @"vao")
      ∙ bindBufferObject (SSym @"vbo") (SSym @"vao")
      ∙ allocateBufferObject
        (SSym @"vbo")
        (SSym @"vao")
        GL.StaticDraw
        [ GL.Vertex3 @GL.GLfloat (-0.5) (-0.5) 0, -- vertex 1 position
          GL.Vertex3 0 1 0, -- vertex 1 color
          GL.Vertex3 0 0 0, -- vertex 1 texture coord
          GL.Vertex3 0.5 (-0.5) 0, -- vertex 2 position
          GL.Vertex3 1 0 0, -- vertex 2 color
          GL.Vertex3 1 0 0, -- vertex 2 texture coord
          GL.Vertex3 0 0.5 0, -- vertex 3 position
          GL.Vertex3 0 0 1, -- vertex 3 color
          GL.Vertex3 0.5 1 0, -- vertex 3 texture coord
          GL.Vertex3 (-0.5) (-0.5) (-0.2), -- back vertex 1 position
          GL.Vertex3 0 1 0, -- back vertex 1 color
          GL.Vertex3 0 0 0, -- back vertex 1 texture coord
          GL.Vertex3 0.5 (-0.5) (-0.2), -- back vertex 2 position
          GL.Vertex3 1 0 0, -- vertex 2 color
          GL.Vertex3 1 0 0, -- vertex 2 texture coord
          GL.Vertex3 0 0.5 (-0.2), -- back vertex 3 position
          GL.Vertex3 0 0 1, -- back vertex 3 color
          GL.Vertex3 0.5 1 0 -- back vertex 3 texture coord
        ]
      ∙ bindElementArray (SSym @"ebo") (SSym @"vao")
      ∙ allocateElementArray
        (SSym @"ebo")
        (SSym @"vao")
        GL.StaticDraw
        [ 0 :: GL.GLuint,
          1,
          2, -- front triangle
          3,
          4,
          5, -- back triangle
          1,
          4,
          5, -- first triangle right side
          1,
          5,
          2, -- second triangle right side
          0,
          3,
          2, -- first triangle left side
          3,
          5,
          2 -- second triangle right side
        ]
      ∙ attribPointerFloat (SNat @0) (SSym @"vao") 3 (Just 9) Nothing
      ∙ enableAttribPointer (SNat @0) (SSym @"vao")
      ∙ attribPointerFloat (SNat @1) (SSym @"vao") 3 (Just 9) (Just 3)
      ∙ enableAttribPointer (SNat @1) (SSym @"vao")
      ∙ attribPointerFloat (SNat @2) (SSym @"vao") 3 (Just 9) (Just 6)
      ∙ enableAttribPointer (SNat @2) (SSym @"vao")
      ∙ unbindBufferObject (SSym @"vbo")
      ∙ unbindArrayObject (SSym @"vao")
      ∙ unbindElementArray (SSym @"ebo") (SSym @"vao")
      ∙ useProgram (SSym @"singleColorTriangle")
      ∙ setUniform (SSym @"singleColorTriangle") "color" (GL.Vertex3 @GL.GLfloat 0.137 0.42 0.557)
      ∙ unuseProgram (SSym @"singleColorTriangle")

updateMultiColorTrianglePos :: TrackT (StateT SceneState IO) i i (GL.GLmatrix GL.GLfloat)
updateMultiColorTrianglePos = do
  st <- (↑) get
  t <- (↑!) $ readTVarIO (view lastFrame st) <&> double2Float
  let rot = axisAngle @GL.GLfloat (V3 0 1 0) (pi / 2 * sin t)
  (↑!) $ glMatrix @GL.GLfloat (H.translate (V3 0.45 0.5 (-0.2)) !*! H.rotate rot !*! H.scale (V3 0.5 0.5 0.5))

run :: GLFW.Window -> TrackT (StateT SceneState IO) _ _ () -> TrackT (StateT SceneState IO) _ _ ()
run window f = do
  q <- (↑!) $ GLFW.windowShouldClose window
  (↑!) $ GL.clearColor GL.$= GL.Color4 0.1 0.1 0.1 1.0
  (↑!) $ GL.depthFunc GL.$= Just GL.Less
  (↑!) $ GL.clear [GL.ColorBuffer, GL.DepthBuffer]
  st <- (↑) get
  (↑!) $ GLFW.setKeyCallback window $ Just (keyCallback $ view pressedKeys st)
  (↑!) $ GLFW.setCursorInputMode window GLFW.CursorInputMode'Disabled
  (↑!) $ GLFW.setCursorPosCallback window $ Just (mouseCallback (view mouseSensitivity st) (view frameDelta st) (view cursorPosition st) (view eulerAngles st))
  f
  (↑!) $ GLFW.swapBuffers window
  (↑!) GLFW.pollEvents
  t <- (↑!) getTime <&> fromJust
  (↑!) $ updateCounters t st
  (↑!) $ showFps st "HaskenGL Scene 01" window
  unless q $ run window f
