#version 430 core

layout (location = 0) in vec3 iPos;

layout (std140, row_major) uniform matrices {
  mat4 view;
  mat4 projection;
};

void main() {
  gl_Position = projection * view * vec4(iPos, 1.0);
}
