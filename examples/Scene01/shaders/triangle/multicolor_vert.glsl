#version 430 core

layout (location = 0) in vec3 iPos;
layout (location = 1) in vec3 iColor;

layout (std140, row_major) uniform matrices {
  mat4 view;
  mat4 projection;
};

out vec3 oColor;

uniform mat4 model;

void main() {
  gl_Position = projection * view * model * vec4(iPos, 1.0);
  oColor = iColor;
}
