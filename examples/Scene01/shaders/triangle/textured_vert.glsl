#version 430 core

layout (location = 0) in vec3 pos;
layout (location = 2) in vec2 tex;

layout (std140, row_major) uniform matrices {
  mat4 view;
  mat4 projection;
};

uniform mat4 model;

out vec2 texCoord;

void main() {
  gl_Position = projection * view * model * vec4(pos, 1.0);
  texCoord = tex;
}
