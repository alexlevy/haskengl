#version 430 core

in vec2 texCoord;

uniform sampler2D container;

out vec4 fragColor;

void main() {
  fragColor = texture(container, texCoord);
}
